import { Elysia } from "elysia";
import { html } from "@elysiajs/html";
import { cors } from "@elysiajs/cors";

const elysia = new Elysia();
elysia.use(html());
elysia.use(cors());

const baseHTML = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Angelie</title>
    <meta charset="UTF-8">
    <script src="https://unpkg.com/htmx.org@1.9.10" integrity="sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body
    hx-get="http://localhost:3000/h"
    hx-trigger="load"
    hx-swap="innerHTML"
  >
  </body>
</html>
`;

elysia.get("/", () => {
  return baseHTML;
});

elysia.get("/h", () => {
  return <h1>Hello, this is base message!</h1>;
});

elysia.listen(3000, () => {
  console.log(`App is listening on port 3000`);
});
